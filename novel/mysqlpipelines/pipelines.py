#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 10:12
# @Author  : Zhangyu
# @Site    : 
# @File    : pipelines.py
# @Software: PyCharm
from .sql import Sql
from novel.items import NovelItem, ContentItem, UpdateItem


class NovelPipeline(object):

    def process_item(self, items, spider):
        if isinstance(items, NovelItem):
            name_id = items['name_id']
            ret = Sql.select_name(name_id)
            if ret == 1:
                print(u"已经存在了")
                pass
            else:
                category = items['category']
                novel_name = items['novel_name']
                author = items['author']
                novel_url = items['novel_url']
                serial_status = items['serial_status']
                serial_number = items['serial_number']
                collection_num = items['collection_num']
                click_num = items['click_num']
                recommend_num = items['recommend_num']
                latest_update_time = items['latest_update_time']
                Sql.insert_novel(name_id, category, novel_name, author, novel_url, serial_status, serial_number,
                                 collection_num, click_num, recommend_num, latest_update_time)
                print("complete info")

        if isinstance(items, ContentItem):
            name_id = items['name_id']
            chapter_num = items['chapter_num']
            chapter_name = items['chapter_name']
            chapter_url = items['chapter_url']
            chapter_content = items['chapter_content']
            Sql.insert_content(name_id, chapter_num, chapter_name, chapter_url, chapter_content)
            print("complete content")

        if isinstance(items, UpdateItem):
            novelName = items['novelName']
            chapter = items['chapter']
            chapterName = items['chapterName']
            # ret = Sql.select_is_update(chapter)
            # if ret == 1:
            #     print('已经存在 {}'.format(chapterName))
            #     pass
            # else:
            Sql.insert_updateNovel(novelName, chapter, chapterName)
            print('complete {}'.format(chapterName))
