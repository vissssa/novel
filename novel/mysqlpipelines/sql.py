#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 10:12
# @Author  : Zhangyu
# @Site    : 
# @File    : sql.py
# @Software: PyCharm
import MySQLdb
from novel import settings

mysql_con = MySQLdb.connect(settings.HOSTS, settings.USERS, settings.PASSWORD, settings.DB, charset='utf8')
cursor = mysql_con.cursor()


class Sql:

    @classmethod
    def insert_novel(cls, name_id, category, novel_name, author, novel_url, serial_status, serial_number,
                     collection_num, click_num, recommend_num, latest_update_time):
        sql = 'INSERT INTO novel (name_id, category, novel_name, author, novel_url, serial_status, ' \
              'serial_number,collection_num, click_num, recommend_num, latest_update_time) VALUES ' \
              '(%(name_id)s,%(category)s,%(novel_name)s,%(author)s,%(novel_url)s,%(serial_status)s,%(serial_number)s,' \
              '%(collection_num)s,%(click_num)s,%(recommend_num)s,%(latest_update_time)s)'
        value = {
            'name_id': int(name_id),
            'category': category,
            'novel_name': novel_name,
            'author': author,
            'novel_url': novel_url,
            'serial_status': serial_status,
            'serial_number': serial_number,
            'collection_num': collection_num,
            'click_num': click_num,
            'recommend_num': recommend_num,
            'latest_update_time': latest_update_time
        }
        cursor.execute(sql, value)
        mysql_con.commit()

    @classmethod
    def insert_content(cls, name_id, chapter_num, chapter_name, chapter_url, chapter_content):
        sql = 'INSERT INTO novel_content ( name_id, chapter_num, chapter_name, chapter_url, chapter_content) VALUES ' \
              '(%(name_id)s, %(chapter_num)s, %(chapter_name)s, %(chapter_url)s, %(chapter_content)s)'
        value = {
            'name_id': int(name_id),
            'chapter_num': int(chapter_num),
            'chapter_name': chapter_name,
            'chapter_url': chapter_url,
            'chapter_content': chapter_content,
        }
        cursor.execute(sql, value)
        mysql_con.commit()

    @classmethod
    def insert_updateNovel(cls, novelName, chapter, chapterName):
        sql = 'INSERT INTO updateNovel(novelName, chapter, chapterName) VALUES (%(novelName)s,%(chapter)s,%(chapterName)s)'
        value = {
            'novelName': novelName,
            'chapter': chapter,
            'chapterName': chapterName,
        }
        cursor.execute(sql, value)
        mysql_con.commit()

    @classmethod
    def select_is_update(cls, chapter):
        sql = 'SELECT EXISTS(SELECT 1 FROM updateNovel WHERE chapter=%(chapter)s)'
        value = {
            'chapter': chapter
        }
        cursor.execute(sql, value)
        return cursor.fetchall()[0][0]

    @classmethod
    def select_name(cls, name_id):
        sql = 'SELECT EXISTS(SELECT 1 FROM novel WHERE name_id=%(name_id)s)'
        value = {
            'name_id': name_id
        }
        cursor.execute(sql, value)
        return cursor.fetchall()[0][0]

    @classmethod
    def select_chapterurl(cls, chapter_url):
        sql = 'SELECT EXISTS(SELECT 1 FROM novel_content WHERE chapter_url=%(chapter_url)s)'
        value = {
            'chapter_url': chapter_url
        }
        cursor.execute(sql, value)
        return cursor.fetchall()[0][0]

    @classmethod
    def close(cls):
        cursor.close()
        mysql_con.close()
