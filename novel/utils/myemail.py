#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-27 9:40
# @Author  : Zhangyu
# @Site    : 
# @File    : myemail.py
# @Software: PyCharm
from email.header import Header
from email.mime.text import MIMEText
from email.utils import parseaddr, formataddr
import smtplib

'''
发送邮件
'''


class MyEmail:
    def __init__(self, chapterName, content):
        self.content = content
        self.chapterName = chapterName

    def _format_addr(self, s):
        name, addr = parseaddr(s)
        return formataddr((Header(name, 'utf-8').encode(), addr))

    def sendEmail(self):
        from_addr = '18056004315@163.com'
        password = 'zhangyu292724306'
        to_addr = '292724306@qq.com'
        smtp_server = 'smtp.163.com'

        html = ''
        for i in self.content:
            html += i

        msg = MIMEText(html, 'html', 'utf-8')
        msg['From'] = self._format_addr('{} <%s>'.format(self.chapterName) % from_addr)
        msg['To'] = self._format_addr('张宇 <%s>' % to_addr)
        msg['Subject'] = Header('{}'.format(self.chapterName), 'utf-8').encode()

        server = smtplib.SMTP_SSL(smtp_server, 465)
        server.set_debuglevel(1)
        server.login(from_addr, password)
        server.sendmail(from_addr, [to_addr], msg.as_string())
        server.quit()
