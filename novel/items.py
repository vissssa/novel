# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class NovelItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # 小说名字
    novel_name = scrapy.Field()
    # 小说作者
    author = scrapy.Field()
    # 小说地址
    novel_url = scrapy.Field()
    # 状态
    serial_status = scrapy.Field()
    # 连载字数
    serial_number = scrapy.Field()
    # 文章类别
    category = scrapy.Field()
    # 小说编号
    name_id = scrapy.Field()
    # 最后更新时间
    latest_update_time = scrapy.Field()
    # 收藏数
    collection_num = scrapy.Field()
    # 点击数
    click_num = scrapy.Field()
    # 推荐数
    recommend_num = scrapy.Field()


class ContentItem(scrapy.Item):
    name_id = scrapy.Field()
    chapter_num = scrapy.Field()
    chapter_name = scrapy.Field()
    chapter_url = scrapy.Field()
    chapter_content = scrapy.Field()


class UpdateItem(scrapy.Item):
    novelName = scrapy.Field()
    chapter = scrapy.Field()
    chapterName = scrapy.Field()
