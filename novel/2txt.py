#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 15:30
# @Author  : Zhangyu
# @Site    : 
# @File    : 2txt.py
# @Software: PyCharm
import MySQLdb
from novel import settings

mysql_con = MySQLdb.connect(settings.HOSTS, settings.USERS, settings.PASSWORD, settings.DB, charset='utf8')
cursor = mysql_con.cursor()
sql = 'SELECT chapter_name,chapter_content FROM novel_content where name_id=26612 ORDER BY chapter_num'
cursor.execute(sql)
contents = cursor.fetchall()
for content in contents:
    with open(u'莽荒纪.txt', 'a') as f:
        f.write(content[0] + '\n')
    content_list = content[1].split('<br/><br/>')
    for i in content_list:
        with open(u'莽荒纪.txt', 'a') as f:
            f.write(i + '\n')
