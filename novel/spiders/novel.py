#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-20 17:57
# @Author  : Zhangyu
# @Site    : 
# @File    : novel.py
# @Software: PyCharm
import scrapy
import re
from scrapy.http import Request
from novel.items import NovelItem, ContentItem
from bs4 import BeautifulSoup
from novel.mysqlpipelines.sql import Sql
import time


class Myspider(scrapy.Spider):
    name = 'novel'
    allowed_domains = ['x23us.com']
    bash_url = 'https://www.x23us.com/class/'
    bashurl = '.html'

    def start_requests(self):
        for i in range(1, 11):
            url = self.bash_url + str(i) + '_1' + self.bashurl
            yield Request(url, self.parse)
        yield Request('https://www.x23us.com/quanben/1', callback=self.parse)

    def parse(self, response):
        soup = BeautifulSoup(response.text, 'lxml')
        max_num = soup.find('div', id='pagelink').find_all('a')[-1].get_text(strip=True)
        bashurl = str(response.url)[:-7]
        for num in range(1, int(max_num) + 1):
            url = bashurl + "_" + str(num) + self.bashurl
            yield Request(url, callback=self.get_name)

    def get_name(self, response):
        soup = BeautifulSoup(response.text, 'lxml')
        trs = soup.find_all('tr', bgcolor='#FFFFFF')
        for tr in trs:
            tds = tr.find_all('td')
            novel_name = tds[0].find_all('a')[1].get_text(strip=True)
            novel_url = tds[0].find_all('a')[0]['href']
            author = tds[2].get_text()
            yield Request(novel_url, callback=self.get_chapterurl,
                          meta={'novel_name': novel_name, 'novel_url': novel_url, 'author': author})

    def get_chapterurl(self, response):
        items = NovelItem()
        soup = BeautifulSoup(response.text, 'lxml')
        table = soup.find('table', id='at')
        items['novel_name'] = response.meta['novel_name']
        items['author'] = response.meta['author']
        items['novel_url'] = response.meta['novel_url']
        items['category'] = table.find('a').get_text(strip=True)
        items['serial_status'] = table.find_all('tr')[0].find_all('td')[-1].get_text(strip=True)
        items['serial_number'] = table.find_all('tr')[1].find_all('td')[1].get_text(strip=True)
        items['name_id'] = str(response.url).split('/')[-1]
        items['latest_update_time'] = table.find_all('tr')[1].find_all('td')[-1].get_text(strip=True)
        items['collection_num'] = table.find_all('tr')[1].find_all('td')[0].get_text(strip=True)
        items['click_num'] = table.find_all('tr')[2].find_all('td')[0].get_text(strip=True)
        items['recommend_num'] = table.find_all('tr')[3].find_all('td')[0].get_text(strip=True)
        # 爬取小说信息
        yield items
        if int(items['collection_num']) > 3000:
            # 爬取小说内容(以蛮荒记为例)
            bashurl = soup.find('p', class_='btnlinks').find('a', class_='read')['href']
            yield Request(bashurl, callback=self.get_chapter_list, meta={'name_id': items['name_id']})

    def get_chapter_list(self, response):
        urls = re.findall(r'<td class="L"><a href="(.*?)">(.*?)</a></td>',
                          response.text)
        print(urls)
        chapter_num = 0
        for url in urls:
            chapter_num += 1
            chapter_url = str(response.url) + url[0]
            chapter_name = url[1]
            ret = Sql.select_chapterurl(chapter_url)
            if ret == 1:
                print(u'该章节已经存在了')
                pass
            else:
                yield Request(chapter_url, callback=self.get_content,
                              meta={'chapter_num': chapter_num, 'chapter_url': chapter_url,
                                    'chapter_name': chapter_name,
                                    'name_id': response.meta['name_id']})

    def get_content(self, response):
        items = ContentItem()
        items['chapter_num'] = response.meta['chapter_num']
        items['chapter_url'] = response.meta['chapter_url']
        items['chapter_name'] = response.meta['chapter_name']
        items['name_id'] = response.meta['name_id']
        content = BeautifulSoup(response.text, 'lxml').find('dd', id='contents')
        items['chapter_content'] = str(content).replace('<dd id="contents">', '').replace(
            '</dd>', '')
        return items
