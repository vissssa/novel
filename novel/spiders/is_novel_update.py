#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-27 8:52
# @Author  : Zhangyu
# @Site    : 
# @File    : is_novel_update.py
# @Software: PyCharm
import scrapy
from novel.items import UpdateItem
from novel.mysqlpipelines.sql import Sql
from scrapy.http import Request
from novel.utils.myemail import MyEmail


class is_novel_update_spider(scrapy.Spider):
    name = 'isUpdate'
    allowed_domains = ['dingdiann.com']
    start_urls = ['https://m.dingdiann.com/ddk38807/']

    def parse(self, response):
        items = UpdateItem()
        items['novelName'] = response.xpath('/html/body/header/span/text()').extract_first()
        chapterList = response.xpath('//*[@id="chapterlist"]/p')
        for chapter in chapterList:
            chapterUrl = chapter.xpath('a/@href').extract_first()
            items['chapterName'] = chapter.xpath('a/text()').extract_first()
            items['chapter'] = str(chapterUrl).split('/')[-1].split('.')[0]
            ret = Sql.select_is_update(items['chapter'])
            if ret == 1:
                print('已经存在 {}'.format(items['chapterName']))
                pass
            else:
                yield items
                contentUrl = response.urljoin(items['chapter'] + '.html')
                yield Request(contentUrl, meta={'chapterName': items['chapterName']}, callback=self.getContent)
        Sql.close()

    def getContent(self, response):
        content = response.xpath('//*[@id="chaptercontent"]').extract()
        chapterName = response.meta['chapterName']
        myemail = MyEmail(chapterName, content)
        myemail.sendEmail()
