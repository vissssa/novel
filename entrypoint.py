#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018-04-21 8:58
# @Author  : Zhangyu
# @Site    : 
# @File    : entrypoint.py
# @Software: PyCharm
from scrapy.cmdline import execute

execute(['scrapy', 'crawl', 'isUpdate'])
